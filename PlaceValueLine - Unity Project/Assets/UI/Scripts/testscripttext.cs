﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
 
public class testscripttext : MonoBehaviour
{
    public TMP_Text[] textObjects;
    public List<float> fontSizes = new List<float>();
    public GridManager gridMan;
 
    void Start()
    {

    }
    
    public void ChangeTextSize()
    {
        foreach(TMP_Text i in textObjects)
        {
            if(i.gameObject.transform.parent.gameObject.activeSelf == true)
            {
                i.enableAutoSizing = false;

                i.fontSize = fontSizes[0];

                i.color = new Color32(0,0,0,255);
            }
        }
    }

    public void CheckTextSize()
    {
        fontSizes = new List<float>();
        
        foreach(TMP_Text i in textObjects)
        {
            if(i.gameObject.transform.parent.gameObject.activeSelf == true)
            {
                i.enableAutoSizing = true;
                i.color = new Color32(0,0,0,0);
            }
        }
        Invoke("SortTextSizes", 0.01f);
    }

    public void SortTextSizes()
    {
        foreach (TMP_Text i in textObjects)
        {
            if(i.gameObject.transform.parent.gameObject.activeSelf == true)
            {
                fontSizes.Add(i.fontSize);
            }
        }

        fontSizes.Sort();

        Invoke("ChangeTextSize",0.01f);
    }

    
}


 
