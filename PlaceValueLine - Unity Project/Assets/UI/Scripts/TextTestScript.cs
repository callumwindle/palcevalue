using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextTestScript : MonoBehaviour
{
    public GridManager gridManager;
    public List<Text> txtBoxes, tempList;
    public GameObject placeHolderText;
    public List<int> bestFontSizes;
    public bool isActive;
    public int minSize, bestFontSize, listSize;
    public string tempText;
    public Transform tempParent;
         
    void Start () 
    {
        StartCoroutine(SetTextSize());
        gridManager.RefreshDisplay();    
    }
            
    IEnumerator SetTextSize ()
    {
        //Need to delay a frame
        yield return null;

        ResetText();
    }
    
    //Set smallest
    void SetSmallest()
    {
        for (int i = 0; i < txtBoxes.Count; i++)
        {
            txtBoxes[i].resizeTextMaxSize = bestFontSizes[0];
            Debug.Log(minSize);
        }
    }

    //Find all font sizes & sort
    void FindFontSizes()
    {
       for(int i = 0; i < txtBoxes.Count; i++)
        {
            if(txtBoxes[i].gameObject.transform.parent.gameObject.activeSelf == true)
            {
                bestFontSizes.Add(txtBoxes[i].cachedTextGenerator.fontSizeUsedForBestFit);
            }
        }

        bestFontSizes.Sort();

        SetSmallest(); 
    }
    
    public void ResetText()
    {
        bestFontSizes = new List<int>(); 
        tempList = new List<Text>();
     
        for(int i = 0; i < txtBoxes.Count; i++)
        {
            Debug.Log(i);
            if(txtBoxes[i].gameObject.transform.parent.gameObject.activeSelf == true)
            {
                tempText = txtBoxes[i].text;
                tempParent = txtBoxes[i].gameObject.transform.parent.gameObject.transform;
                
                Instantiate(placeHolderText, tempParent, false);

                tempParent.GetChild(1).gameObject.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, 0);
                tempParent.GetChild(1).gameObject.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, 0);
                tempParent.GetChild(1).gameObject.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, 0);
                tempParent.GetChild(1).gameObject.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0, 0);

                tempParent.GetChild(1).gameObject.GetComponent<RectTransform>().anchorMax = new Vector2(1,1);
                tempParent.GetChild(1).gameObject.GetComponent<RectTransform>().anchorMin = new Vector2(0,0);

                tempParent.GetChild(1).gameObject.GetComponent<Text>().resizeTextMaxSize = 100;

                tempParent.GetChild(1).gameObject.GetComponent<Text>().text = tempText;
                tempList.Add(tempParent.GetChild(1).gameObject.GetComponent<Text>());
                 
            }
            else
            {
                Instantiate(txtBoxes[i].gameObject, txtBoxes[i].gameObject.transform.parent.gameObject.transform, true);
                tempList.Add(txtBoxes[i].gameObject.transform.parent.gameObject.transform.GetChild(1).gameObject.GetComponent<Text>());
            }
        }

        foreach (Text x in txtBoxes)
        {
            Destroy(x.gameObject);
        }

        txtBoxes = tempList;

        FindFontSizes();
    }

    

}
