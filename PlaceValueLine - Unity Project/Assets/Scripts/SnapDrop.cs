﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SnapDrop : MonoBehaviour, IDropHandler
{
    // Start is called before the first frame update
    private RectTransform slotRect, eventDataRect;
    public int counterAmt, i, x, childs, leftNextPos, rightNextPos, currentColumn;
    public int columnSwitcher, loopTrigger, childAmount, startingContainer;
    private GameObject placeHolderObject, leftColumn, rightColumn, dcColumn, currentContainer; 
    public GameObject currentChild, placeHolderTemplate, gridTotal;
    private string currentChildName;
    private Text gridTotalText;
    private string counterAmtText;
    public Color trasnsparentColour;
    public bool leftIsFull;
    public Color counterColour;

    public GameObject testX, testY, testZ;
    
    void Start()
    {
        slotRect = GetComponent<RectTransform>();
        columnSwitcher = 0;
        gridTotalText = gridTotal.GetComponent<Text>();
        ResetColumn();      
    }

    public void OnDrop(PointerEventData eventData)
    {
        eventData.pointerDrag.GetComponent<DragDrop>().inGrid = true;
        
        if(eventData.pointerDrag != null && counterAmt < 9)
        {
            counterAmt++;
            eventDataRect = eventData.pointerDrag.GetComponent<RectTransform>();
            leftColumn = transform.GetChild(0).gameObject;
            rightColumn = transform.GetChild(1).gameObject;

            if(counterAmt == 9 && leftIsFull == true)
            {
                eventData.pointerDrag.transform.SetParent(rightColumn.transform.GetChild(rightNextPos).transform, true);
                columnSwitcher++;
                rightNextPos++;
            }
            else if(columnSwitcher >= 0)
            {       
                eventData.pointerDrag.transform.SetParent(leftColumn.transform.GetChild(leftNextPos).transform, true);
                leftNextPos++;
                columnSwitcher--;

                if(leftNextPos == 4)
                {
                    leftIsFull = true;
                }
                
            }
            else if(columnSwitcher < 0 && counterAmt != 9)
            {
                eventData.pointerDrag.transform.SetParent(rightColumn.transform.GetChild(rightNextPos).transform, true);
                columnSwitcher++;
                rightNextPos++;
            }
            
            eventDataRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, 0);
            eventDataRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, 0);
            eventDataRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, 0);
            eventDataRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0, 0);
            eventDataRect.anchorMax = new Vector2(1,1);
            eventDataRect.anchorMin = new Vector2(0,0);
            eventData.pointerDrag.GetComponent<Image>().color = counterColour;

            UpdateTotal();

        }
        else
        {
            Destroy(eventData.pointerDrag);
        }
    }

    public void ResetColumn()
    {
        for(i = 0; i < 2; i++)
        {
            currentChild = transform.GetChild(i).gameObject;
            childs = currentChild.transform.childCount;
            
            for(x = 0; x < childs; x++)
            {
                currentChild.transform.GetChild(x).gameObject.GetComponent<Image>().color = trasnsparentColour;
                if(currentChild.transform.GetChild(x).gameObject.transform.childCount > 0)
                {
                    Destroy(currentChild.transform.GetChild(x).gameObject.transform.GetChild(0).gameObject);
                }
            }
        }
        counterAmt = 0;
        rightNextPos = 0;
        leftNextPos = 0;
        leftIsFull = false;
        UpdateTotal();
    }

    public void UpdateTotal()
    {
        counterAmtText = counterAmt.ToString();
        gridTotalText.text = counterAmtText;
    }

    public void ConnectCounters(int editedColumn)
    {
        currentColumn = columnSwitcher;
        
        switch(editedColumn)
        {
            case 1:
                dcColumn = gameObject.transform.GetChild(0).gameObject;
                loopTrigger = 3;
            break;

            case 2:
                dcColumn = gameObject.transform.GetChild(1).gameObject;
                loopTrigger = 4;
            break;
        }
        
        for(x=0; x<loopTrigger; x++)
        {
            currentContainer = dcColumn.transform.GetChild(x).gameObject;
            if(currentContainer.transform.childCount == 0 && dcColumn.transform.GetChild(x+1).gameObject.transform.childCount != 0)
            {
                dcColumn.transform.GetChild(x+1).gameObject.transform.GetChild(0).gameObject.transform.SetParent(currentContainer.transform, false);
            }
        }

        columnSwitcher = currentColumn;
        UpdateTotal();
    }
}

