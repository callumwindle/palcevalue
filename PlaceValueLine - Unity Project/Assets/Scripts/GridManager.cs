﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public List<GameObject> gridListA, gridListB, titleList, decimalList = new List<GameObject>();
    public GameObject textManager;
    private int minVisible, maxVisible, i, childs, x;
    private GameObject currentObjectA, currentObjectB, currentTitle;
    private bool decToggle;
    public testscripttext textScript;
    
    // Start is called before the first frame update
    void Start()
    {
        minVisible = 3;
        maxVisible = 7;
        decToggle = true;
    }

    public void EditGrid(int buttonPressed)
    {
        switch(buttonPressed)
        {
            case 1:
                if(minVisible != 0)
                {
                    minVisible--;
                }
            break;

            case -1:
                if(minVisible != 7)
                {
                    minVisible++;
                }
            break;

            case 2:
                {
                    decToggle = !decToggle;
                }
            break;

        }
        
        RefreshDisplay();
    }

    public void RefreshDisplay()
    {
        for(i=0; i < minVisible; i++)
        {
            currentObjectA = gridListA[i];
            currentObjectA.SetActive(false);
            currentObjectB = gridListB[i];
            currentObjectB.SetActive(false);
            currentTitle = titleList[i];
            currentTitle.SetActive(false);
            currentObjectA.GetComponent<SnapDrop>().ResetColumn();
            currentObjectB.GetComponent<SnapDrop>().ResetColumn();
        }

        for(i=minVisible; i <= 7; i++)
        {
            currentObjectA = gridListA[i];
            currentObjectA.SetActive(true);
            currentObjectB = gridListB[i];
            currentObjectB.SetActive(true);
            currentTitle = titleList[i];
            currentTitle.SetActive(true);
        }

        if(decToggle)
        {
            for(i=8; i < 12; i++)
            {
                currentObjectA = gridListA[i];
                currentObjectA.SetActive(false);
                currentObjectB = gridListB[i];
                currentObjectB.SetActive(false);
                currentTitle = titleList[i];
                currentTitle.SetActive(false);
                if(i > 8)
                {
                    currentObjectA.GetComponent<SnapDrop>().ResetColumn();
                    currentObjectB.GetComponent<SnapDrop>().ResetColumn();
                }    
            }
        }
        else
        {
            for(i=8; i < 12; i++)
            {
                currentObjectA = gridListA[i];
                currentObjectA.SetActive(true);
                currentObjectB = gridListB[i];
                currentObjectB.SetActive(true);
                currentTitle = titleList[i];
                currentTitle.SetActive(true);
            }
        }

        textScript.CheckTextSize();
    }

    public void EmptyColumns()
    {
        for(i=0; i<12; i++)
        {
            if(i != 8)
            {
                currentObjectA = gridListA[i];
                currentObjectA.GetComponent<SnapDrop>().ResetColumn();

                currentObjectB = gridListB[i];
                currentObjectB.GetComponent<SnapDrop>().ResetColumn();
            }
        }
    }
}
