﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleTotals : MonoBehaviour
{
    public List<GameObject> textList = new List<GameObject>();
    private bool isVisible;
    // Start is called before the first frame update
    void Start()
    {
        isVisible = false;
        ToggleTotal();
    }

    public void ToggleTotal()
    {
        if(isVisible)
        {
            foreach(GameObject i in textList)
            {
                i.SetActive(false);
            }
            isVisible = false;
        }
        else
        {
            foreach(GameObject i in textList)
            {
                i.SetActive(true);
            }
            isVisible = true;
        }
    }
}
