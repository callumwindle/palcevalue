using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IPointerUpHandler
{
    private RectTransform rectTransform;
    public Canvas canvas;
    private CanvasGroup canvasGroup;
    private GameObject placeHolderObject;
    public GameObject counterTemplate, placeHolderTemplate;
    private GameObject currentParentObj;
    public SnapDrop snapDrop;
    public string currentParent;
    private bool hasDuplicated;
    public bool inGrid, virgin, hasDragged;
    
    void Start()
    {
        hasDragged = false;
        virgin = false;
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        hasDuplicated = false;
        inGrid = false;
    }

    void FixedUpdate()
    {
        // if(virgin && !inGrid)
        // {
        //     Destroy(gameObject);
        // }
    }
    
    public void OnPointerDown(PointerEventData eventData)
    {
        if(!hasDuplicated)
        {
            GameObject duplicate = Instantiate(counterTemplate);
            duplicate.transform.SetParent(canvas.GetComponent<RectTransform>(), false);
            hasDuplicated = true;
        }
        
        if(inGrid)
        {
            currentParentObj = gameObject.transform.parent.gameObject;
            currentParent = currentParentObj.transform.parent.name;
            snapDrop = currentParentObj.transform.parent.gameObject.transform.parent.gameObject.GetComponent<SnapDrop>();

            GameObject placeHolderObject = Instantiate(placeHolderTemplate);
            snapDrop.counterAmt--;
            gameObject.transform.SetParent(canvas.transform, true);
            inGrid = false;
            switch(currentParent)
            {
                case "LeftColumn":
                    snapDrop.leftNextPos--;
                    snapDrop.columnSwitcher++;
                    snapDrop.ConnectCounters(1);
                    snapDrop.leftIsFull = false;
                break;

                case "RightColumn":
                    snapDrop.rightNextPos--;
                    snapDrop.columnSwitcher--;
                    snapDrop.ConnectCounters(2);
                break;
            }

        }
        
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        hasDragged = true;
        canvasGroup.blocksRaycasts = false;
        canvasGroup.alpha = 0.75f;
    }

    public void OnDrag(PointerEventData eventData)
    {
        rectTransform.anchoredPosition += (eventData.delta/canvas.scaleFactor);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        virgin = true;
        if(inGrid)
        {
            canvasGroup.blocksRaycasts = true;
            canvasGroup.alpha = 1f;
            hasDragged = false;
            
        }
        else
        {
            Destroy(gameObject);
        }
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(virgin && !inGrid && !hasDragged)
        {
            Destroy(gameObject);
        }
    }
}
